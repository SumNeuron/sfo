# make latest version
```
python setup.py sdist bdist_wheel
```

# submit latest version
```
python -m twine upload  dist/*<version>*
python -m twine upload --repository-url https://upload.pypi.org/legacy/ dist/<version>*
```
